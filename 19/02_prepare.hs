import Data.List (nub, sort)
import Data.List.Split (splitOn)
import Data.Map (Map, delete, empty, insert, (!))

data Rule = L Char | R [[Int]] | RR Int | RRR (Int, Int) deriving (Show)

-- lengths - [24,32,40,48,56,64,72,80,88,96]
-- possible counters for RR and RRR (base formula is (xxxxxxxx)(yyyyyyyy|zzzzzzz)), there is group of 8 xes, multiplied by RR, and 8yes with 8 zes, multiplied by RRR
-- 24: (1,1); 32: (2,1); 40: (3,1), (1,2); 48: (4,1), (2,2); 56: (5,1), (3,2), (1, 3); 64: (6,1), (4,2), (2,3); ... 88: (1,5), (3,4), (4,3), (5,3), (7,2)
-- 24: 124; 32: 1; 40: 0; 48: 0; 56: 0; 64: 0; 88: 0;

main = do
  lines <- readLines "input.txt"
  let ruleLines = takeWhile (not . null) lines
  let msgLines = drop 1 $ dropWhile (not . null) lines
  let msgLines' = filter (\l -> length l == 32) msgLines
  print $ length msgLines'
  let rules = getRules ruleLines
  print $ length rules
  print $ length (filter (`elem` rules) msgLines')

-- let lengths = sort $ nub $ map length msgLines
-- let msgLineGroups = map (\l -> filter (\m -> length m == l) msgLines) lengths
-- print $ map length msgLineGroups
-- let rules = getRules ruleLines
-- print $ length rules
-- let ruleGroups = map (\l -> filter (\r -> length r == l) rules) lengths
-- print $ map length ruleGroups
-- let matchinglines = zipWith (\m r -> length $ filter (`elem` r) m) msgLineGroups ruleGroups
-- print matchinglines
-- print $ sum matchinglines

readLines = fmap lines . readFile

getRules lines =
  let m = foldl (\acc l -> insert (read (head $ splitOn ": " l) :: Int) (parse $ splitOn ": " l !! 1) acc) empty lines
      m' = insert 8 (RR 42) m
      m'' = insert 11 (RRR (42, 31)) m'
   in travel1 m'' (m'' ! 0) [[]] -- ++ travel2 m'' (m'' ! 0) [[]] -- ++ travel3 m'' (m'' ! 0) [[]] -- ++ travel4 m'' (m'' ! 0) [[]]++ travel5 m'' (m'' ! 0) [[]]

parse "\"a\"" = L 'a'
parse "\"b\"" = L 'b'
parse str =
  let x = map (map read . words) (splitOn " | " str)
   in R x

travel1 m r str = case r of
  L c -> map (\s -> s ++ [c]) str
  R v -> concatMap (foldl (\acc x -> travel1 m (m ! x) acc) str) v
  RR x ->
    let str' = travel1 m (m ! x) [[]]
     in [s ++ s' ++ s'' | s <- str, s' <- str', s'' <- str']  -- tu jest 128 * 128 = 16k opcji !!!
     -- musimy zrobic inaczej - dla tych o długosci 32 sprawdzac pierwszą 8 i drugą 8, czy sa w zbiorze ósemek, a nastepnie pozostałą czesc, czy są w zbiorze 16 !!
  RRR (x, y) ->
    let str' = travel1 m (m ! x) [[]]
        str'' = travel1 m (m ! y) [[]]
     in [s ++ concat (replicate 1 s') ++ concat (replicate 1 s'') | s <- str, s' <- str', s'' <- str'']

travel2 m r str = case r of
  L c -> map (\s -> s ++ [c]) str
  R v -> concatMap (foldl (\acc x -> travel2 m (m ! x) acc) str) v
  RR x ->
    let str' = travel2 m (m ! x) [[]]
     in [s ++ concat (replicate 1 s') | s <- str, s' <- str']
  RRR (x, y) ->
    let str' = travel2 m (m ! x) [[]]
        str'' = travel2 m (m ! y) [[]]
     in [s ++ concat (replicate 2 s') ++ concat (replicate 2 s'') | s <- str, s' <- str', s'' <- str'']

travel3 m r str = case r of
  L c -> map (\s -> s ++ [c]) str
  R v -> concatMap (foldl (\acc x -> travel3 m (m ! x) acc) str) v
  RR x ->
    let str' = travel3 m (m ! x) [[]]
     in [s ++ concat (replicate r s') | s <- str, s' <- str', r <- [2 .. 2]]
  RRR (x, y) ->
    let str' = travel3 m (m ! x) [[]]
        str'' = travel3 m (m ! y) [[]]
     in [s ++ concat (replicate r s') ++ concat (replicate r s'') | s <- str, s' <- str', s'' <- str'', r <- [3 .. 3]]

travel4 m r str = case r of
  L c -> map (\s -> s ++ [c]) str
  R v -> concatMap (foldl (\acc x -> travel4 m (m ! x) acc) str) v
  RR x ->
    let str' = travel4 m (m ! x) [[]]
     in [s ++ concat (replicate 5 s') | s <- str, s' <- str']
  RRR (x, y) ->
    let str' = travel4 m (m ! x) [[]]
        str'' = travel4 m (m ! y) [[]]
     in [s ++ concat (replicate 3 s') ++ concat (replicate 3 s'') | s <- str, s' <- str', s'' <- str'']

travel5 m r str = case r of
  L c -> map (\s -> s ++ [c]) str
  R v -> concatMap (foldl (\acc x -> travel5 m (m ! x) acc) str) v
  RR x ->
    let str' = travel5 m (m ! x) [[]]
     in [s ++ concat (replicate 7 s') | s <- str, s' <- str']
  RRR (x, y) ->
    let str' = travel5 m (m ! x) [[]]
        str'' = travel5 m (m ! y) [[]]
     in [s ++ concat (replicate 2 s') ++ concat (replicate 2 s'') | s <- str, s' <- str', s'' <- str'']