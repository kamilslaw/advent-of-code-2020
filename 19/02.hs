import Data.List (nub, sort)
import Data.List.Split (splitOn)
import Data.Map (Map, delete, empty, insert, (!))

data Rule = L Char | R [[Int]] | RR Int | RRR (Int, Int) deriving (Show)

main = do
  lines <- readLines "input.txt"
  let ruleLines = takeWhile (/= "\r") lines
  let msgLines = drop 1 $ dropWhile (/= "\r") lines
  let msgLines' = map (filter (/= '\r')) msgLines
  let rules = getRules ruleLines
  print $ length (filter (isCorrect rules) msgLines')

getMultipliers l = [(x, y) | x <- [1 .. 10], y <- [1 .. 5], x * 8 + y * 16 == l]

isCorrect rules line =
  let multipliers = getMultipliers $ length line
   in any (isMultiplierCorrect rules line) multipliers

isMultiplierCorrect (a, (b, c)) line (m1, m2) =
  let aOk = all (\m -> take 8 (drop ((m - 1) * 8) line) `elem` a) [1 .. m1]
      bOk = all (\m -> take 8 (drop ((m - 1) * 8) (drop (m1 * 8) line)) `elem` b) [1 .. m2]
      cOk = all (\m -> take 8 (drop ((m - 1) * 8) (drop (m2 * 8) (drop (m1 * 8) line))) `elem` c) [1 .. m2]
   in aOk && bOk && cOk

readLines = fmap lines . readFile

getRules lines =
  let m = foldl (\acc l -> insert (read (head $ splitOn ": " l) :: Int) (parse $ splitOn ": " l !! 1) acc) empty lines
      m' = insert 8 (RR 42) m
      m'' = insert 11 (RRR (42, 31)) m'
      rr = travelRR m'' (m'' ! 0) [[]]
      rrr = travelRRR m'' (m'' ! 0) [[]]
   in (rr, splitAt 128 rrr)

parse "\"a\"\r" = L 'a'
parse "\"b\"\r" = L 'b'
parse str =
  let x = map (map read . words) (splitOn " | " str)
   in R x

travelRR m r str = case r of
  L c -> map (\s -> s ++ [c]) str
  R v -> concatMap (foldl (\acc x -> travelRR m (m ! x) acc) str) v
  RR x -> travelRR m (m ! x) [[]]
  RRR (x, y) -> str

travelRRR m r str = case r of
  L c -> map (\s -> s ++ [c]) str
  R v -> concatMap (foldl (\acc x -> travelRRR m (m ! x) acc) str) v
  RR x -> str
  RRR (x, y) ->
    let str' = travelRRR m (m ! x) [[]]
        str'' = travelRRR m (m ! y) [[]]
     in str' ++ str''