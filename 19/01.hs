import Data.List.Split (splitOn)
import Data.Map (Map, delete, empty, insert, (!))

data Rule = L Char | R [[Int]] deriving (Show)

main = do
  lines <- readLines "input.txt"
  let ruleLines = takeWhile (not . null) lines
  let msgLines = drop 1 $ dropWhile (not . null) lines
  let rules = getRules ruleLines
  print $ length (filter (`elem` rules) msgLines)

readLines = fmap lines . readFile

getRules lines =
  let m = foldl (\acc l -> insert (read (head $ splitOn ": " l) :: Int) (parse $ splitOn ": " l !! 1) acc) empty lines
   in travel m (m ! 0) [[]]

parse "\"a\"" = L 'a'
parse "\"b\"" = L 'b'
parse str =
  let x = map (map read . words) (splitOn " | " str)
   in R x

travel m r str = case r of
  L c -> map (\s -> s ++ [c]) str
  R v -> concatMap (foldl (\acc x -> travel m (m ! x) acc) str) v