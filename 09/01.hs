type Number = Int

type Sums = [Int]

data State = State Number Sums deriving (Show)

main = do
  lines <- readLines "input.txt"
  let states = foldl parse [] (take 25 lines)
  let corrupted = checkNext states (drop 25 lines)
  print corrupted

readLines = fmap lines . readFile

parse states line =
  let number = read line
      sums = map (\(State n _) -> n + number) states
      state = State number sums
      states' = states ++ [state]
   in states'

removeLast states =
  let states' = tail states
      states'' = map (\(State n v) -> State n (tail v)) states'
   in states''

isInState number = any (\(State _ v) -> number `elem` v)

checkNext _ [] = error "!!"
checkNext states (line : lines) =
  let number = read line
      isOk = isInState number states
   in ( if isOk
          then checkNext (removeLast $ parse states line) lines
          else number
      )
