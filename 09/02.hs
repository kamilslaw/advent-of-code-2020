corrupted = 1124361034

main = do
  lines <- readLines "input.txt"
  let values = map parse lines
  let result = perform 0 0 (head values) values
  print result

readLines = fmap lines . readFile

parse x = read x :: Int

perform l r sum values =
  let r' = r + 1
      l' = l + 1
      sum' = sum + (values !! r')
   in case compare sum' corrupted of
        EQ -> getResult l r' values
        LT -> perform l r' sum' values
        GT -> perform l' l' (values !! l') values

getResult l r values =
  let values' = take (r - l + 1) (drop l values)
      left = minimum values'
      right = maximum values'
   in ((left, right), left + right)