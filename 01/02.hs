readLines = fmap lines . readFile

parse path = do
  content <- readLines path
  return (map read content)

main = do
  values <- parse "input.txt"
  return $ head [i * j * k | i <- values, j <- values, k <- values, i + j + k == 2020]