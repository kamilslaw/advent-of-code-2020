readLines = fmap lines . readFile

parse path = do
  content <- readLines path
  return (map read content)

main = do
  values <- parse "input.txt"
  return $ head [i * j | i <- values, j <- values, i + j == 2020]