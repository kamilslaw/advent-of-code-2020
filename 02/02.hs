{-# LANGUAGE RecordWildCards #-}

import Text.Regex.PCRE ((=~))

data Line = Line {range :: (Int, Int), letter :: Char, value :: String} deriving (Show)

readLines = fmap lines . readFile

regex = "(\\d+)-(\\d+) (\\w): (\\w+)"

parse :: String -> Line
parse str =
  let group = head (str =~ regex :: [[String]])
      left = read $ group !! 1
      right = read $ group !! 2
      letter = head $ group !! 3
      value = group !! 4
   in Line {range = (left, right), letter = letter, value = value}

getCorrectCount = length . filter isLineCorrect

isLineCorrect :: Line -> Bool
isLineCorrect Line {..} =
  let leftIsCorrect = letter == value !! (fst range - 1)
      rightIsCorrect = letter == value !! (snd range - 1)
   in leftIsCorrect /= rightIsCorrect

main = do
  linesStr <- readLines "input.txt"
  let lines = map parse linesStr
  print $ getCorrectCount lines