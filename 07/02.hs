{-# LANGUAGE TupleSections #-}

import Data.List (elemIndex, sort)
import Data.Maybe (fromJust)
import Text.Regex.PCRE ((=~))

main = do
  lines <- readLines "input.txt"
  let names = parseNames lines
  let bounds = (0, length names - 1)
  let edges = parseEdges lines names
  let graph = buildGraph bounds edges
  let result = getChildrenCount graph (getIndex names "shiny gold")
  print result

readLines = fmap lines . readFile

bagNameRegex = "^(.+?) bags"

targetBagsNamesRegex = "((\\d) (\\D+?) bag)+"

parseNames = sort . map parseName

parseName :: String -> String
parseName line =
  let match = line =~ bagNameRegex :: [[String]]
   in head match !! 1

getIndex names name = fromJust $ elemIndex name names

toInts names (from, (toVal, toName)) = (getIndex names from, (toVal, getIndex names toName))

parseEdges lines names = map (toInts names) (concatMap parseEdge lines)

parseEdge line =
  let from = parseName line
      to = parseTargets line
   in map (from,) to

parseTargets :: String -> [(Int, String)]
parseTargets line =
  let match = line =~ targetBagsNamesRegex :: [[String]]
   in map (\x -> (read (x !! 2), x !! 3)) match

buildGraph (_, to) edges = map (buildGraphPart edges) [0 .. to]

buildGraphPart edges vertex = map snd (filter (\x -> fst x == vertex) edges)

getChildrenCount g v = getChildrenCountInternal 1 g v - 1

getChildrenCountInternal count graph vertex =
  let children = (graph !! vertex)
   in if null children then count else count + sum (map (\c -> count * getChildrenCountInternal (fst c) graph (snd c)) children)