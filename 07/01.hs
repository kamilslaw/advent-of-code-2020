{-# LANGUAGE TupleSections #-}

import Data.Graph (buildG, reachable, transposeG)
import Data.List (elemIndex, sort)
import Data.Maybe (fromJust)
import Text.Regex.PCRE ((=~))

main = do
  lines <- readLines "input.txt"
  let names = parseNames lines
  let bounds = (0, length names - 1)
  let edges = parseEdges lines names
  let graph = buildG bounds edges
  let resultVerticles = reachable (transposeG graph) (getIndex names "shiny gold")
  print $ length resultVerticles - 1

readLines = fmap lines . readFile

bagNameRegex = "^(.+?) bags"

targetBagsNamesRegex = "(\\d (\\D+?) bag)+"

parseNames = sort . map parseName

parseName :: String -> String
parseName line =
  let match = line =~ bagNameRegex :: [[String]]
   in head match !! 1

getIndex names name = fromJust $ elemIndex name names

toInts names (from, to) = (getIndex names from, getIndex names to)

parseEdges lines names = map (toInts names) (concatMap parseEdge lines)

parseEdge line =
  let from = parseName line
      to = parseTargets line
   in map (from,) to

parseTargets :: String -> [String]
parseTargets line =
  let match = line =~ targetBagsNamesRegex :: [[String]]
   in map (!! 2) match