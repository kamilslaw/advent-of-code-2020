main = do
  lines <- readLines "input.txt"
  let result = map (parse . compute . convert . clearSpaces) lines
  print $ sum result

readLines = fmap lines . readFile

clearSpaces [] = []
clearSpaces (' ' : a) = clearSpaces a
clearSpaces (a : b) = a : clearSpaces b

convert str = convertImpl (str, ([], []))

convertImpl ([], (stack, op)) = stack ++ op
convertImpl (h : t, (stack, op)) = case h of
  '(' -> convertImpl (t, (stack, h : op))
  '+' ->
    let (stack', op') = moveToOperators h stack op
     in convertImpl (t, (stack', op'))
  '*' ->
    let (stack', op') = moveToOperators h stack op
     in convertImpl (t, (stack', op'))
  ')' ->
    let (stack', op') = moveToStack stack op
     in convertImpl (t, (stack', op'))
  _ -> convertImpl (t, (stack ++ [h], op))

moveToStack stack ('(' : t) = (stack, t)
moveToStack stack (h : t) = moveToStack (stack ++ [h]) t

moveToOperators c stack [] = (stack, [c])
moveToOperators c stack ('(' : t) = (stack, [c, '('] ++ t)
moveToOperators c stack (h : t) = moveToOperators c (stack ++ [h]) t

compute stack = concat (snd $ computeImpl (stack, []))

computeImpl ([], l) = ([], l)
computeImpl (h : t, r) = case h of
  '+' ->
    let a = head r
        b = r !! 1
        a' = read a :: Integer
        b' = read b :: Integer
     in computeImpl (t, show (a' + b') : drop 2 r)
  '*' ->
    let a = head r
        b = r !! 1
        a' = read a :: Integer
        b' = read b :: Integer
     in computeImpl (t, show (a' * b') : drop 2 r)
  _ -> computeImpl (t, [h] : r)

parse str = read str :: Int