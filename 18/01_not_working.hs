import Text.ParserCombinators.Parsec (parse, (<|>))
import Text.ParserCombinators.Parsec.Language (GenLanguageDef (reservedOpNames), emptyDef)
import qualified Text.ParserCombinators.Parsec.Token as P

main = do
  formulasStr <- readLines "input_test.txt"
  let formulas = map parseFormula formulasStr
  print formulas
  let result = map execute formulas
  print $ sum result

readLines = fmap lines . readFile

parseFormula str =
  let --str' = "(" ++ str ++ ")"
      str' = clearSpaces str
   in case parse statement "" str' of
        Left e -> error $ show e
        Right r -> r

clearSpaces [] = []
clearSpaces (' ' : a) = clearSpaces a
clearSpaces (a : b) = a : clearSpaces b

execute formula = case formula of
  Value i -> i
  Operator Sum f1 f2 -> execute f1 + execute f2
  Operator Product f1 f2 -> execute f1 * execute f2
  Brackets f -> execute f

data Formula
  = Value Integer
  | Operator Op Formula Formula
  | Brackets Formula
  deriving (Show)

data Op = Sum | Product deriving (Show)

languageDef = emptyDef {reservedOpNames = ["+", "*"]}

lexer = P.makeTokenParser languageDef

integer = P.integer lexer

reservedOp = P.reservedOp lexer

reserved = P.reserved lexer

-- parens = P.parens lexer

int = do Value <$> integer

subFormula m = do
  reserved "("
  n <- m
  reserved ")"
  return $ Brackets n

operator = (reservedOp "+" >> return Sum) <|> (reservedOp "*" >> return Product)

expression m = do
  l <- m
  o <- operator
  r <- m
  return $ Operator o l r

statement = subFormula statement <|> int <|> expression statement
