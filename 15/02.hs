import Data.Foldable (foldl')
import Data.Map (Map, delete, elems, empty, insert, lookup)

inputTest = [0, 3, 6]

input = [9, 3, 1, 0, 8, 4]

target = 30000000

main = do
  let v = input
  let startindex = length v
  let m = foldl (\acc (x, i) -> insert x (i, -1) acc) empty (zip v [1 ..])
  let result = foldl' (\acc _ -> playTurn acc) ((m, startindex + 1), last v) [startindex .. (target - 1)]
  print $ snd result

playTurn ((m, i), x) = case Data.Map.lookup x m of
  Nothing -> ((updateM m 0 i, i + 1), 0)
  Just (l, -1) -> ((updateM m 0 i, i + 1), 0)
  Just (l, r) -> ((updateM m (l - r) i, i + 1), l - r)

updateM m x i = case Data.Map.lookup x m of
  Nothing -> insert x (i, -1) m
  Just (l, _) -> insert x (i, l) m
