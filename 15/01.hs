inputTest = [0, 3, 6]

input = [9, 3, 1, 0, 8, 4]

main = do
  let value = input
  let startindex = length value
  let result = foldl (\acc _ -> playTurn acc) value [startindex .. 2019]
  print $ last result

playTurn v =
  let reverseV = reverse v
      last = head reverseV
      len = length v
      prev = search last len reverseV []
      prevLength = length prev
   in if prevLength < 2
        then v ++ [0]
        else v ++ [head prev - (prev !! 1)]

search x i [] res = res
search x i (h : t) res
  | length res == 2 = res
  | x == h = search x (i - 1) t (res ++ [i])
  | otherwise = search x (i - 1) t res