import Data.Set (Set, insert, member, singleton)

data State = State Int Int (Set Int) | Terminated Int

main = do
  lines <- readLines "input.txt"
  case execute lines of
    State {} -> print "error"
    Terminated value -> print value

readLines :: FilePath -> IO [String]
readLines = fmap lines . readFile

execute lines = foldr (\_ acc -> executeStep lines acc) (State 0 0 (singleton 0)) lines

executeStep _ (Terminated value) = Terminated value
executeStep lines (State value position set) =
  let parts = words (lines !! position)
      operation = head parts
      valueStr = parts !! 1
      operationValue =
        if head valueStr == '+'
          then read (tail valueStr) :: Int
          else read valueStr
      nextValue =
        if operation == "acc"
          then value + operationValue
          else value
      nextPosition =
        if operation == "jmp"
          then position + operationValue
          else position + 1
   in if member nextPosition set
        then Terminated nextValue
        else State nextValue nextPosition (insert nextPosition set)