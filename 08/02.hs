import Data.Set (Set, insert, member, singleton)

data State = State Int Int (Set Int) | Terminated Int | Broken

isTerminated (Terminated _) = True
isTerminated _ = False

main = do
  lines <- readLines "input.txt"
  case tryEexecute lines of
    Terminated value -> print value
    _ -> print "error"

readLines :: FilePath -> IO [String]
readLines = fmap lines . readFile

tryEexecute lines = head (filter isTerminated (map (\i -> execute $ replaceLine i lines) [0 .. (length lines - 1)]))

replaceLine i lines =
  let parts = words (lines !! i)
      operation = head parts
      valueStr = parts !! 1
      (l, r) = splitAt i lines
   in case operation of
        "acc" -> lines
        "jmp" -> l ++ ("nop " ++ valueStr) : tail r
        "nop" -> l ++ ("jmp " ++ valueStr) : tail r

execute lines = foldr (\_ acc -> executeStep lines acc) (State 0 0 (singleton 0)) lines

executeStep _ Broken = Broken
executeStep _ (Terminated value) = Terminated value
executeStep lines (State value position set) =
  let parts = words (lines !! position)
      operation = head parts
      valueStr = parts !! 1
      operationValue =
        if head valueStr == '+'
          then read (tail valueStr) :: Int
          else read valueStr
      nextValue =
        if operation == "acc"
          then value + operationValue
          else value
      nextPosition =
        if operation == "jmp"
          then position + operationValue
          else position + 1
   in if member nextPosition set
        then Broken
        else
          if nextPosition >= length lines
            then Terminated nextValue
            else State nextValue nextPosition (insert nextPosition set)