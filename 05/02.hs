import Data.Bits
import Data.List (foldl')

-- https://stackoverflow.com/a/26961027
toDec :: String -> Int
toDec = foldl' (\acc x -> acc * 2 + readBit x) 0

readBit b
  | b == 'B' = 1
  | b == 'F' = 0
  | b == 'R' = 1
  | b == 'L' = 0

readLines = fmap lines . readFile

-- https://stackoverflow.com/a/28885929
ushiftR :: Int -> Int -> Int
ushiftR n k = fromIntegral (fromIntegral n `shiftR` k :: Word)

getRow dec = ushiftR dec 3

getColumn dec = dec .&. 7

main = do
  lines <- readLines "input.txt"
  let values = map toDec lines
  let ids = map (\x -> (8 * getRow x) + getColumn x) values
  let range = [(minimum ids) .. (maximum ids)]
  print $ head (filter (`notElem` ids) range)