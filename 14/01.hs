import Data.Char (intToDigit, isDigit)
import Data.Map (Map, delete, elems, insert, singleton, (!))
import Data.Maybe (fromMaybe)
import Numeric (showIntAtBase)

maskKey = 0

main = do
  lines <- readLines "input.txt"
  let m = foldl compute (singleton maskKey "") lines
  let values = elems $ delete maskKey m
  print $ sum (map bin2dec values)

readLines = fmap lines . readFile

compute m line =
  let parts = words line
      isMaskUpdate = head parts == "mask"
      value = parts !! 2
   in if isMaskUpdate
        then updateMask value m
        else updateValue (getIndex $ head parts) value m

updateMask = insert maskKey

getIndex str = read (takeWhile isDigit (dropWhile (not . isDigit) str)) :: Int -- "map[X]"

updateValue i value m =
  let mask = m ! maskKey
      value' = read value :: Int
      value'' = showIntAtBase 2 intToDigit value' ""
      value''' = replicate (36 - length value'') '0' ++ value''
   in insert i (computeValue value''' mask) m

computeValue = zipWith computeBit

computeBit v 'X' = v
computeBit _ '1' = '1'
computeBit _ '0' = '0'

bin2dec = foldr (\c s -> s * 2 + c) 0 . reverse . map c2i
  where
    c2i c = if c == '0' then 0 else 1
