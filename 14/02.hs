import Data.Char (intToDigit, isDigit)
import Data.Map (Map, delete, elems, insert, singleton, (!))
import Data.Maybe (fromMaybe)
import Numeric (showIntAtBase)

maskKey = 0

main = do
  lines <- readLines "input.txt"
  let m = foldl compute (singleton maskKey "") lines
  let values = elems $ delete maskKey m
  print $ sum (map bin2dec values)

readLines = fmap lines . readFile

compute m line =
  let parts = words line
      isMaskUpdate = head parts == "mask"
      value = parts !! 2
   in if isMaskUpdate
        then updateMask value m
        else updateValue (getIndex $ head parts) value m

updateMask = insert maskKey

getIndex str = read (takeWhile isDigit (dropWhile (not . isDigit) str)) :: Int -- "map[X]"

updateValue i value m =
  let mask = m ! maskKey
      value' = read value :: Int
      value'' = showIntAtBase 2 intToDigit value' ""
      value''' = replicate (36 - length value'') '0' ++ value''
      value'''' = computeValue value''' mask
      addresses = getAddresses i mask
   in foldl (\m' a -> insert a value'''' m') m addresses

computeValue = zipWith computeBit

computeBit v _ = v
-- computeBit v 'X' = v
-- computeBit _ '1' = '1'
-- computeBit _ '0' = '0'

getAddresses i mask =
  let i' = showIntAtBase 2 intToDigit i ""
      i'' = replicate (36 - length i') '0' ++ i'
      addressMask = zipWith computeAddressBit i'' mask
      addresses = gatherAdresses addressMask [[]]
   in map bin2dec addresses

computeAddressBit _ 'X' = 'X'
computeAddressBit _ '1' = '1'
computeAddressBit v '0' = v

gatherAdresses [] result = result
gatherAdresses (h : t) result = case h of
  'X' ->
    let l = map (\x -> x ++ ['0']) result
        r = map (\x -> x ++ ['1']) result
     in gatherAdresses t (l ++ r)
  _ -> gatherAdresses t (map (\x -> x ++ [h]) result)

bin2dec = foldr (\c s -> s * 2 + c) 0 . reverse . map c2i
  where
    c2i c = if c == '0' then 0 else 1
