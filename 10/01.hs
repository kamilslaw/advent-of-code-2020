import Data.List (sort)

main = do
  lines <- readLines "input.txt"
  let values = prepare $ map parse lines
  let result = travel 0 0 values
  print result

readLines = fmap lines . readFile

parse x = read x :: Int

prepare values =
  let values' = sort values
   in [0] ++ values' ++ [last values' + 3]

travel one three [_] = one * three
travel one three (f : t) = case head t - f of
  3 -> travel one (three + 1) t
  1 -> travel (one + 1) three t
  _ -> travel one three t