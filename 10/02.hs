import Data.List (sort)

-- Result was (value means number of possible moves from that position):
-- [3,2,1,1,3,3,2,1,1,3,2,1,1,1,3,2,1,1,1,1,1,1,1,1,3,2,1,1,2,1,1,1,3,2,1,1,1,1,3,3,2,1,1,1,1,3,2,1,1,1,1,2,1,1,1,3,2,1,1,2,1,1,3,3,2,1,1,3,3,2,1,1,1,3,2,1,1,1,3,2,1,1,1,1,3,3,2,1,1,2,1,1,1,1,3,3,2,1,1,3,2,1,1,0]
-- then for each group i calculate:
-- [2] = 2, [3,2] = 4, [3,3,2] = 7 possible moves
-- then i multiply all groups together - 4 * 7 * 4 * 4 * 4 * 2 * ... = 1973822685184

main = do
  lines <- readLines "input.txt"
  let values = prepare $ map parse lines
  let moves = getMoves values
  print moves

readLines = fmap lines . readFile

parse x = read x :: Int

prepare values =
  let values' = sort values
   in [0] ++ values' ++ [last values' + 3]

getMoves values =
  let max = maximum values
      postfixValue = [max + 4, max + 4, max + 4]
      range = [0 .. (length values - 1)]
      values' = values ++ postfixValue -- to avoid out of range error
   in map (numberOfMoves values') range

numberOfMoves values i =
  let v = values !! i
      f = values !! (i + 1)
      f' = if f - v <= 3 then 1 else 0
      s = values !! (i + 2)
      s' = if s - v <= 3 then 1 else 0
      t = values !! (i + 3)
      t' = if t - v <= 3 then 1 else 0
   in f' + s' + t'

-- DOES NOT WORK -- i've checked it manually, no need to fix
-- travel _ value [_] = value
-- travel subPath value (f : t) =
--   if multiStep f
--     then travel (subPath ++ [f]) value t
--     else travel [] (value * getSubPathValue subPath) t

-- multiStep = (> 1)

-- getSubPathValue subPath = case length subPath of
--   0 -> 1 -- []
--   1 -> 2 -- [2]
--   2 -> 4 -- [3, 2]
--   3 -> 7 -- [3, 3, 2]