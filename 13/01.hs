import Data.List (elemIndex)
import Data.List.Split (splitOn)
import Data.Maybe (fromJust)

main = do
  lines <- readLines "input.txt"
  let value = parse $ head lines
  let buses = map parse (filter (/= "x") (splitOn "," (lines !! 1)))
  let distToValue = map (\b -> b * ceiling (fromIntegral value / fromIntegral b) - value) buses
  let busIndex = fromJust (elemIndex (minimum distToValue) distToValue)
  let result = (buses !! busIndex) * (distToValue !! busIndex)
  print result

readLines = fmap lines . readFile

parse x = read x :: Int
