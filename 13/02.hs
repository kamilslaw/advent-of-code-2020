-- https://pl.wikipedia.org/wiki/Chi%C5%84skie_twierdzenie_o_resztach
-- https://sites.google.com/site/topinfo12/home/arytmetyka-modulo/rownania-modularne
-- http://ww2.ii.uj.edu.pl/~wilczak/ilo/pdf/twierdzenie_chinskie.pdf

-- 17,x,13,19 is 3417
-- x ≡ 3 (mod 17)
-- x ≡ 1 (mod 13)
-- x ≡ 0 (mod 19)
-- 1) x = 3 + 17i : x mod 13 = 1 : i = 6 : 3 + 17*6 = 105
-- 2) x ≡ 105 (mod 17 * 13) ≡ 105 (mod 221)
-- 3) x = 105 + 221*i : x mod 19 = 0 : i = 15 : 105 + 221*15 = 3420
-- 4) 3420 - 3 = 3417 !!

import Data.List.Split (splitOn)

getI l r i a b =
  let x = l + (r * i)
   in if x `mod` a == b then i else getI l r (i + 1) a b

compute x acc (_, value) [] = x
compute x acc (_, value) ((index', value') : t) =
  let i = getI x acc 0 value' index'
      x' = x + acc * i
   in compute x' (acc * value') (index', value') t

readLines = fmap lines . readFile

normalize x str = foldl (\str h -> str ++ [if h == "x" then "0" else h]) str x

parse x = read x :: Int

main = do
  lines <- readLines "input_test.txt"
  let buses = map parse (normalize (splitOn "," (lines !! 1)) [])
  let x = length buses - 1
  let values = filter (\x -> snd x > 0) (getValues x buses [])
  let result = compute x (snd $ head values) (head values) (tail values)
  print $ result - x

getValues i [] res = res
getValues i (h : t) res = getValues (i - 1) t (res ++ [(i, h)])
