import Data.List.Split (splitOn)

data Tile = T Int [String] [String] deriving (Show) -- id, borders, reversed borders

main = do
  lines <- readLines "input.txt"
  let tiles = parse lines []
  let similarities = map (findSimilar tiles) tiles
  let corners = filter (\(_, (b, rB)) -> sum b == 2 || sum rB == 2) similarities
  print corners
  print $ product $ map fst corners

readLines = fmap lines . readFile

parse [] tiles = tiles
parse ("\r" : t) tiles = parse t tiles
parse (h : t) tiles =
  let id = read (words (head (splitOn ":" h)) !! 1)
      tileLines = take 10 t
      borders = [map head tileLines, head tileLines, map (!! 9) tileLines, tileLines !! 9]
      borders' = map (filter (/= '\r')) borders
      tile = T id borders' (map reverse borders')
   in parse (drop 10 t) (tile : tiles)

findSimilar tiles (T id b rB) = (id, (map (similarCount tiles id) b, map (similarCount tiles id) rB))

similarCount tiles bId border = length $ filter (\(T id b rB) -> id /= bId && (border `elem` b || border `elem` rB)) tiles