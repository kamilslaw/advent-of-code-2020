readLines = fmap lines . readFile

slopes = [(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)]

withTree (right, down) (index, line) =
  let position = (index * right) `div` down
      offset = position `mod` length line
   in index `mod` down == 0 && line !! offset == '#'

main = do
  lines <- readLines "input.txt"
  let linesWithIndex = zip [0 ..] lines
  let linesWithoutFirst = tail linesWithIndex
  let counts = map (\x -> length $ filter (withTree x) linesWithoutFirst) slopes
  print counts
  print $ product counts