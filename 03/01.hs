readLines = fmap lines . readFile

withTree (index, line) =
  let position = index * 3
      offset = position `mod` length line
   in line !! offset == '#'

main = do
  lines <- readLines "input.txt"
  let linesWithIndex = zip [0 ..] lines
  let count = length $ filter withTree (tail linesWithIndex)
  print count