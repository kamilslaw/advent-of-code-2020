import Data.List (intersect, sort)
import Data.List.Split (splitOn)

requiredFields = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"]

main = do
  txt <- readFile "input.txt"
  let lines = splitOn "\n\n" txt
  let passports = map toPassport lines
  print $ getCorrectCount passports

toPassport = map (head . splitOn ":") . words

getCorrectCount = length . filter isPassportCorrect

isPassportCorrect p = areEqual (p `intersect` requiredFields) requiredFields

areEqual a b = sort a == sort b