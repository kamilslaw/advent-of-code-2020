import Data.List (intersect, sort)
import Data.List.Split (splitOn)
import Text.Regex.PCRE ((=~))

requiredFields = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"]

main = do
  txt <- readFile "input.txt"
  let lines = splitOn "\n\n" txt
  let passports = map toPassport (filter isPassport lines)
  print $ getCorrectCount passports

isPassport = all (isOk . splitOn ":") . words

toPassport = map (head . splitOn ":") . words

getCorrectCount = length . filter isPassportCorrect

isPassportCorrect p = areEqual (p `intersect` requiredFields) requiredFields

areEqual a b = sort a == sort b

isOk [k, v]
  | k == "byr" = isByrOk v
  | k == "iyr" = isIyrOk v
  | k == "eyr" = isEyrOk v
  | k == "hgt" = isHgtOk v
  | k == "hcl" = isHclOk v
  | k == "ecl" = isEclOk v
  | k == "pid" = isPidOk v
  | otherwise = True

isByrOk v =
  let match = v =~ "^\\d{4}$" :: [[String]]
      isNumber = not $ null match
      value = if isNumber then read v else 0
   in isNumber && value > 1919 && value < 2003

isIyrOk v =
  let match = v =~ "^\\d{4}$" :: [[String]]
      isNumber = not $ null match
      value = if isNumber then read v else 0
   in isNumber && value > 2009 && value < 2021

isEyrOk v =
  let match = v =~ "^\\d{4}$" :: [[String]]
      isNumber = not $ null match
      value = if isNumber then read v else 0
   in isNumber && value > 2019 && value < 2031

isHgtOk :: [Char] -> Bool
isHgtOk v =
  let match = v =~ "^(\\d{2,3})(cm|in)$" :: [[String]]
      isMatched = not $ null match
   in (isMatched && parseHgt (head match))

parseHgt match =
  let value = read $ match !! 1
      unit = match !! 2
   in case unit of
        "cm" -> value > 149 && value < 194
        "in" -> value > 58 && value < 77

isHclOk :: [Char] -> Bool
isHclOk v = not $ null (v =~ "^#[0-9a-f]{6}$" :: [[String]])

ecls = ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"]

isEclOk v = v `elem` ecls

isPidOk :: [Char] -> Bool
isPidOk v = not $ null (v =~ "^\\d{9}$" :: [[String]])