import Data.List (elemIndex, find)
import Data.List.Split (splitOn)
import Data.Maybe (fromJust)

departureIndexes = [0 .. 5]

main = do
  lines <- readLines "input.txt"
  let paramsLine = takeWhile (not . null) lines
  let paramsName = map (head . splitOn ": ") paramsLine
  let paramsTail = map (\x -> splitOn ": " x !! 1) paramsLine
  let ranges = map (map parseRange . splitOn " or ") paramsTail
  let flatRanges = concat ranges
  let nearbyIndex = fromJust $ elemIndex "nearby tickets:" lines
  let ticketsLine = drop (nearbyIndex + 1) lines
  let tickets = map (map (\s -> read s :: Integer) . splitOn ",") ticketsLine
  let indexedRanges = zip ranges [0 ..]
  let goodTickets = transpose $ filter (inRange flatRanges) tickets
  let resultIndexes = map (check indexedRanges) goodTickets
  let resultIndexesReduced = concat $ travel [] resultIndexes
  let yourTicketIndex = fromJust $ elemIndex "your ticket:" lines
  let yourTicketLine = lines !! (yourTicketIndex + 1)
  let yourTicket = map (\s -> read s :: Integer) (splitOn "," yourTicketLine)
  let values = map (\i -> yourTicket !! fromJust (elemIndex i resultIndexesReduced)) departureIndexes
  print $ product values

readLines = fmap lines . readFile

parseRange str =
  let parts = splitOn "-" str
   in (read $ head parts :: Integer, read (parts !! 1) :: Integer)

inRange flatRanges = all (\t -> any (inSingleRange t) flatRanges)

inSingleRange x (l, r) = x >= l && x <= r

transpose ([] : _) = []
transpose x = map head x : transpose (map tail x)

check indexedRanges column =
  let result = filter (\r -> inRange (fst r) column) indexedRanges
   in map snd result

travel removed resultIndexes =
  let next = find (\x -> length x == 1 && notElem (head x) removed) resultIndexes
   in case next of
        Nothing -> resultIndexes
        Just v -> travel (head v : removed) (remove (head v) resultIndexes)

remove v = map (\r -> if length r == 1 then r else filter (/= v) r)