import Data.List (elemIndex)
import Data.List.Split (splitOn)
import Data.Maybe (fromJust)

main = do
  lines <- readLines "input.txt"
  let paramsLine = takeWhile (not . null) lines
  let paramsTail = map (\x -> splitOn ": " x !! 1) paramsLine
  let ranges = concatMap (map parseRange . splitOn " or ") paramsTail
  let nearbyIndex = fromJust $ elemIndex "nearby tickets:" lines
  let ticketsLine = drop (nearbyIndex + 1) lines
  let tickets = concatMap (map (\s -> read s :: Integer) . splitOn ",") ticketsLine
  let wrongValues = filter (\t -> not (inAnyRange t ranges)) tickets
  print $ sum wrongValues

readLines = fmap lines . readFile

parseRange str =
  let parts = splitOn "-" str
   in (read $ head parts :: Integer, read (parts !! 1) :: Integer)

inAnyRange x = any (inRange x)

inRange x (l, r) = x >= l && x <= r