import Data.Maybe (catMaybes)

data Type = Empty | Occupied | Floor deriving (Show, Eq)

type Pos = (Int, Int)

type Seat = (Type, Pos)

type Seats = [[Seat]]

main = do
  lines <- readLines "input.txt"
  let seats = parse lines
  result <- execute seats 1
  print result

execute seats no = do
  let seats' = getNewSeats seats
  print no
  if noChange seats seats'
    then return $ numberOfOccupied seats
    else execute seats' (no + 1)

readLines = fmap lines . readFile

parse :: [String] -> Seats
parse lines =
  zipWith
    ( curry
        ( \line ->
            zipWith
              (curry (\c -> (parseSeat $ fst c, (snd line, snd c))))
              (fst line)
              [0 ..]
        )
    )
    lines
    [0 ..]

parseSeat 'L' = Empty
parseSeat '.' = Floor

getNewSeats seats = map (map (getNewSeat seats)) seats

getNewSeat seats seat =
  let neighbors = getNeighbors seats seat
      totalTaken = sum (map getSeatValue neighbors)
   in transform seat totalTaken

getNeighbors seats (_, (x, y)) =
  let u = travel seats (x -1) y (-1) 0
      d = travel seats (x + 1) y 1 0
      l = travel seats x (y -1) 0 (-1)
      r = travel seats x (y + 1) 0 1
      ul = travel seats (x -1) (y -1) (-1) (-1)
      ur = travel seats (x -1) (y + 1) (-1) 1
      dl = travel seats (x + 1) (y -1) 1 (-1)
      dr = travel seats (x + 1) (y + 1) 1 1
   in catMaybes [u, d, l, r, ul, ur, dl, dr]

travel seats x y transformX transformY
  | x < 0 || y < 0 || x >= length seats || y >= length (head seats) = Nothing
  | getType seats x y == Floor = travel seats (x + transformX) (y + transformY) transformX transformY
  | otherwise = Just ((seats !! x) !! y)

getSeatValue (Occupied, _) = 1
getSeatValue _ = 0

isEmpty Empty = True
isEmpty _ = False

isOccupied Occupied = True
isOccupied _ = False

transform (t, p) totalTaken
  | totalTaken == 0 && isEmpty t = (Occupied, p)
  | totalTaken >= 5 && isOccupied t = (Empty, p)
  | otherwise = (t, p)

noChange l r = all (all (\(t, (x, y)) -> t == getType r x y)) l

getType seats x y =
  let (t, _) = (seats !! x) !! y
   in t

numberOfOccupied seats = sum (map numberOfOccupiedInRow seats)

numberOfOccupiedInRow = length . filter (\(t, _) -> isOccupied t)