type Pos = (Int, Int)

data Dir = E | S | W | N deriving (Show)

data State = State Pos Dir

main = do
  lines <- readLines "input.txt"
  let state = foldl move (State startPos startDir) lines
  print $ getResult state

readLines = fmap lines . readFile

startDir = E

startPos = (0, 0)

move (State (x, y) dir) line =
  let op = head line
      value = read $ tail line
      dir' =
        if op == 'L' || op == 'R'
          then getNewDir op value dir
          else dir
      op' =
        if op == 'F'
          then head $ show dir'
          else op
      pos' = case op' of
        'E' -> (x + value, y)
        'S' -> (x, y + value)
        'W' -> (x - value, y)
        'N' -> (x, y - value)
        _ -> (x, y)
   in State pos' dir'

getResult (State (x, y) _) = abs x + abs y

getNewDir op value dir =
  let value' = (if op == 'R' then value else 360 - value)
   in case dir of
        E -> case value' of
          90 -> S
          180 -> W
          270 -> N
        S -> case value' of
          90 -> W
          180 -> N
          270 -> E
        W -> case value' of
          90 -> N
          180 -> E
          270 -> S
        N -> case value' of
          90 -> E
          180 -> S
          270 -> W
