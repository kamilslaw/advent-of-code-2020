type Pos = (Int, Int)

type Waypoint = (Int, Int)

data State = State Pos Waypoint

main = do
  lines <- readLines "input.txt"
  let state = foldl move (State startPos startWaypoint) lines
  print $ getResult state

readLines = fmap lines . readFile

startPos = (0, 0)

startWaypoint = (10, -1)

move (State (x, y) (wx, wy)) line =
  let op = head line
      value = read $ tail line
      waypoint' =
        if op == 'L' || op == 'R'
          then transformWaypoint op value (wx, wy)
          else (wx, wy)
      pos' =
        if op == 'F'
          then (x + value * wx, y + value * wy)
          else (x, y)
      waypoint'' = moveWaypoint op value waypoint'
   in State pos' waypoint''

getResult (State (x, y) _) = abs x + abs y

transformWaypoint op value (wx, wy) =
  let value' = (if op == 'R' then value else 360 - value)
   in case value' of
        90 -> (- wy, wx)
        180 -> (- wx, - wy)
        270 -> (wy, - wx)

moveWaypoint op value (wx, wy) = case op of
  'E' -> (wx + value, wy)
  'S' -> (wx, wy + value)
  'W' -> (wx - value, wy)
  'N' -> (wx, wy - value)
  _ -> (wx, wy)