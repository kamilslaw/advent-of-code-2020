import Data.Map (Map, elems, empty, insert, (!))

main = do
  let size = 12
  let indexes = getIndexes size
  m <- getMap size
  let m' = foldl (\acc _ -> perform size acc) m [1 .. 6]
  print $ count '#' (elems m')

getMap size = do
  lines <- readLines "input.txt"
  let indexes = getIndexes size
  let m =
        foldl
          ( \acc (line, yIndex) ->
              foldl
                (\acc2 (c, xIndex) -> insert (xIndex, (yIndex, 0)) c acc2)
                acc
                (zip line [0 ..])
          )
          (foldl (\acc i -> insert i '.' acc) empty indexes)
          (zip lines [0 ..])
  return m

readLines = fmap lines . readFile

getIndexes size = [(x, (y, z)) | x <- [- size .. size], y <- [- size .. size], z <- [- size .. size]]

perform size m = snd (foldl update (m, m) (getIndexes (size - 1)))

update (m, m') (ix, (iy, iz)) =
  let i = (ix, (iy, iz))
      neighbors = [m ! (ix + x, (iy + y, iz + z)) | x <- [- 1, 0, 1], y <- [- 1, 0, 1], z <- [- 1, 0, 1]]
      activeCount = count '#' neighbors
      current = m ! i
      new
        | current == '#' && (activeCount == 3 || activeCount == 4) = '#'
        | current == '.' && activeCount == 3 = '#'
        | otherwise = '.'
   in if current /= new then (m, insert i new m') else (m, m')

count x = length . filter (== x)
