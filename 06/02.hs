import Data.List (intersect)
import Data.List.Split (splitOn)

totalUniquesCount = length . foldl1 intersect

main = do
  txt <- readFile "input.txt"
  let values = map (totalUniquesCount . words) (splitOn "\n\n" txt)
  print $ sum values