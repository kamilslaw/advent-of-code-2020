import Data.Char (isSpace)
import Data.List (nub)
import Data.List.Split (splitOn)

removeWhitespaces = filter (not . isSpace)

uniquesCount = length . nub

main = do
  txt <- readFile "input.txt"
  let values = map (uniquesCount . removeWhitespaces) (splitOn "\n\n" txt)
  print $ sum values